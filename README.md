docker-compose up

Runs on host port 5000

1. Pull the repo:  git pull git@gitlab.com:Cifranic/docker_flask.git
2. cd into the docker_flask repo 
3. Build the image: docker build -t python_flask .
4. Run the contianer using docker compose:  docker-compose up 
5. connect at localhost:5000
