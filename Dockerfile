# start from base
FROM centos:7
MAINTAINER Nick C. <me@hotmail.com>

RUN yum -y install epel-release && \
    yum -y install python2-pip python-devel curl 

# copy our application code
ADD flask-app/ /opt/requirements

# cd into /opt/reguiremnets 
WORKDIR /opt/requirements

# fetch app specific deps 
RUN pip install -r requirements.txt

# create a share between host and container 
VOLUME /opt/flask-app

# working directory when running container
WORKDIR /opt/flask-app


# expose port 
EXPOSE 5000

# start app
CMD [ "python", "./hello.py" ]


